FROM python:3.10

# Get newest version of Mariadb connector
RUN curl -LsS https://r.mariadb.com/downloads/mariadb_repo_setup | bash
RUN apt install -y \
    libmariadb3 \
    libmariadb-dev

WORKDIR /app
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt
