# Python Linting

Python linting Docker image that can easily be used in a pipeline.

Supports sqlalchemy with mariadb


--- 
Original Idea by [mafda](https://mafda.github.io/).

> Check the complete post on [Medium](https://medium.com/semantixbr/how-to-make-your-code-shine-with-gitlab-ci-pipelines-48ade99192d1)
